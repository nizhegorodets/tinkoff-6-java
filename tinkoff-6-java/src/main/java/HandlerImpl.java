import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class HandlerImpl implements Handler {
    @Override
    public ApplicationStatusResponse performOperation(String id) {
        ClientMock mock = new ClientMock();

        // short-lived tasks
        ExecutorService executor = Executors.newCachedThreadPool();

        LinkedList<Callable<Response>> tasks = new LinkedList<>();
        // all failed attempts from any method for performOperation
        AtomicInteger retriesCount = new AtomicInteger();
        // in millis
        AtomicLong lastRequestTime = new AtomicLong();
        tasks.add(new Job(id, 1, 3, mock, retriesCount, lastRequestTime));
        tasks.add(new Job(id, 2, 3, mock, retriesCount, lastRequestTime));

        Response ans = null;

        try {
            ans = executor.invokeAny(tasks, 15, TimeUnit.SECONDS);
        } catch (TimeoutException ex) {
            System.out.println("time-out crash");
        } catch (Exception ex) {
            System.out.println("something went wrong while executing the request");
            System.out.println(ex.getMessage());
        } finally {
            // free resources
            executor.shutdown();
        }

        if (Objects.requireNonNull(ans) instanceof Response.Success success) {
            return new ApplicationStatusResponse.Success(success.applicationStatus(), success.applicationId());
        }
        return new ApplicationStatusResponse.Failure(Duration.of(lastRequestTime.get(), ChronoUnit.MILLIS), retriesCount.get());
    }
}
