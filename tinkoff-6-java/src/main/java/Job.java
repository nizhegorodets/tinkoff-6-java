import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Job implements Callable<Response> {
    private String id;
    private int applicationId;
    private int maxAttempts;
    private ClientMock clientMock;
    private AtomicInteger counter;
    private AtomicLong lastRequestTime;

    public Job(String id, int applicationId, int maxAttempts, ClientMock clientMock, AtomicInteger counter, AtomicLong lastRequestTime) {
        this.id = id;
        this.applicationId = applicationId;
        this.maxAttempts = maxAttempts;
        this.clientMock = clientMock;
        this.counter = counter;
        this.lastRequestTime = lastRequestTime;
    }
    @Override
    public Response call() throws Exception {
        Response res = null;
        for (int i=1; i <= maxAttempts; i++) {
            System.out.printf("start attempt #%d for application id = %d%n", i, applicationId);
            if (applicationId == 1) {
                res = clientMock.getApplicationStatus1(id);
            } else {
                System.out.println();
                res = clientMock.getApplicationStatus2(id);
            }

            switch (res) {
                case Response.Success ignored -> {
                    return res;
                }
                case Response.RetryAfter retryAfter -> {
                    counter.incrementAndGet();
                    lastRequestTime.set(System.currentTimeMillis());
                    System.out.printf("retrying application id = %d with %d delay%n", applicationId,  retryAfter.delay().toMillis());
                    Thread.sleep(retryAfter.delay().toMillis());
                }
                case Response.Failure failed -> {
                    counter.incrementAndGet();
                    lastRequestTime.set(System.currentTimeMillis());
                    System.out.printf("application id = %d failed with \"%s\"%n", applicationId, failed.ex().getMessage());
                }
            }
        }
        return res;
    }
}
