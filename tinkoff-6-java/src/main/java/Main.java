public class Main {
    public static void main(String[] args) {
        Handler handler = new HandlerImpl();
        ApplicationStatusResponse response = handler.performOperation("42");
        System.out.println(response);
    }
}