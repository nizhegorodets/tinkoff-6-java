import java.time.Duration;
import java.util.Random;

public class ClientMock implements Client {
    @Override
    public Response getApplicationStatus1(String id) {
        try {
            int sleepTime = getRandomSleepTime();
            System.out.printf("sleep time for getApplicationStatus1 = %d%n", sleepTime);
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            System.out.println("error during sleeping, id = " + id);
            e.printStackTrace();
        }
        return getResponse(1, id, true);
    }

    @Override
    public Response getApplicationStatus2(String id) {
        try {
            int sleepTime = getRandomSleepTime();
            System.out.printf("sleep time for getApplicationStatus2 = %d%n", sleepTime);
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            System.out.println("error during sleeping, id = " + id);
            e.printStackTrace();
        }
        return getResponse(2, id, true);
    }

    private Response getResponse(int applicationId, String id, boolean isRandom) {
        if (isRandom) {
            int result = new Random().nextInt(0, 3);

            return switch (result) {
                case 0 -> new Response.Success(String.format("%d", applicationId), id);
                case 1 -> new Response.RetryAfter(Duration.ofSeconds(1));
                case 2 -> new Response.Failure(new Throwable("Boom!"));
                default -> throw new IllegalStateException("Unexpected value: " + result);
            };
        } else {
            return new Response.Success(String.format("%d", applicationId), id);
        }
    }

    private int getRandomSleepTime() {
        // 0 to 3 seconds
        return new Random().nextInt(1, 3) * 1000;
    }
}
